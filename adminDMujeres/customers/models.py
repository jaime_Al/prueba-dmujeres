from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.urls import reverse


class Customer(models.Model):
    clientNames = models.CharField(max_length=255)
    clientLastNames = models.CharField(max_length=255)
    identification = models.CharField(max_length=10)
    phoneNumber = models.CharField(max_length=10)
    address = models.CharField(max_length=255)
    status = models.PositiveSmallIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse('blog_post_detail', args=[self.slug])

    class Meta:
        ordering = ['created_at']

        def __unicode__(self):
            return self.title

# Create your models here.
